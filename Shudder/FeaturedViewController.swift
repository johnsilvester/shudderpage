//
//  ViewController.swift
//  Shudder
//
//  Created by John Silvester on 11/15/18.
//  Copyright © 2018 John Silvester. All rights reserved.
//

import UIKit

class FeaturedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView! // Storyboard setup collectionview
    @IBOutlet var featuredViewModel: FeaturedViewModel! //ViewModel where most logic is
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //SetBG color
        collectionView.backgroundColor = UIColor.white
        
        featuredViewModel.registerCellForCollectionView(collectionView: collectionView)
        //Fetch data from API
        featuredViewModel.fetchMoviePosters{movies in
            self.collectionView.reloadData()
            }
    }
    
    //MARK: CollectionView Delegates Required
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return featuredViewModel.dequeCellAtIndexPath(indexPath:indexPath,collectionView:collectionView)
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return featuredViewModel.numberOfItemsInSection(section: section)
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return featuredViewModel.getsizeForCellAtIndexPath(indexPath: indexPath, view: self.view)
        
    }

    



}

