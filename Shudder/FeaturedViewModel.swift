//
//  FeaturedViewModel.swift
//  Shudder
//
//  Created by John Silvester on 11/15/18.
//  Copyright © 2018 John Silvester. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

/**
 View model for controlling most of the business logic related to FeaturedViewController
*/
class FeaturedViewModel: NSObject {
    
    @IBOutlet var moviesClient: MoviesClient!
    var dataIsLoaded = false //used to reload data when images were successfully downloaded
    var movies:[String]?
    private let bannerCellId = "featuredCell"
    private let featuredCellId = "bannerCell"
    private let blankCellId = "blankCell"
    
    /**
     Movie Client Interaction, notifies when the API has grabbed the movie image links successfully
     Completion - list of movie urls in String format
     */
    func fetchMoviePosters(completion: @escaping ([String]) -> ()){
        moviesClient.fetchMovies{movies in
            if (movies.count > 0){
                self.movies = movies
                SavedImageURLS.sharedInstance.urls = movies
                self.dataIsLoaded = true
            }else{
                print("Error Retrieving Movies")
            }
            completion(movies)
        }
    }
    

 //MARK: Main UICollectionView delegate methods

    func numberOfItemsInSection(section: Int) -> Int{
        return 4
    }
    
    func registerCellForCollectionView(collectionView:UICollectionView){
        collectionView.register(FeaturedCell.self, forCellWithReuseIdentifier: featuredCellId)
        collectionView.register(BannerCell.self, forCellWithReuseIdentifier: bannerCellId)
        collectionView.register(BlankCell.self, forCellWithReuseIdentifier: blankCellId)
    }
    
    func getsizeForCellAtIndexPath(indexPath: IndexPath, view:UIView) -> CGSize{
        var size: CGSize
        
        switch indexPath.row{
        case 0:
            size = CGSize(width: view.frame.width, height: 225)
        case _ where indexPath.row > 0:
            size = CGSize(width: view.frame.width, height: 175)
        default:
            size = CGSize(width: view.frame.width, height: 175)
        }

        return size
    }
    
    /**
    displaying cell logic - will only display image cells if images have been downloaded.
     */
    func dequeCellAtIndexPath(indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell{
        var cell: UICollectionViewCell
        
        switch (indexPath.row,dataIsLoaded){
        case (0,true):
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: bannerCellId, for: indexPath) as! BannerCell
        case (1...4,true):
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: featuredCellId, for: indexPath) as! FeaturedCell
        default:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: blankCellId, for: indexPath) as! BlankCell
        }
        
        cell.tag = indexPath.row
        
        return cell
    }
    
}

