//
//  cells.swift
//  Shudder
//
//  Created by John Silvester on 11/15/18.
//  Copyright © 2018 John Silvester. All rights reserved.
//

import UIKit
import AlamofireImage

/**
 Creates custom Large Banner Image View Class, seen at top of app.
 Custom UICollectionViewCell which also creates a UICollectionView Internally
 */
class BannerCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //Initializes models and data client
    private let cellId = "blankCell"
    var featuredViewModel: FeaturedViewModel!
    var moviesClient: MoviesClient!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.moviesClient = MoviesClient.init()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Set internal collectionview properties
    let moviesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    /**
     Adds contraints needed and intiializes inner Blank cell to add movies
     */
    func setupViews() {
        backgroundColor = UIColor.clear
        addSubview(moviesCollectionView)
        
        moviesCollectionView.dataSource = self
        moviesCollectionView.delegate = self
        
        moviesCollectionView.register(BlankCell.self, forCellWithReuseIdentifier: cellId)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": moviesCollectionView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": moviesCollectionView]))
        
    }
    
    // MARK: Inner collectionView delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    /**
     Adds image view to blank cell and add corner radius.
     asynchonrously download image to view.
     Access movie client data to do this via shared instance.
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        let moviePosterImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
        moviePosterImageView.contentMode = .scaleAspectFill
        moviePosterImageView.layer.cornerRadius = 10
        moviePosterImageView.layer.masksToBounds = true
        moviePosterImageView.backgroundColor = UIColor.black
        var urls = SavedImageURLS.sharedInstance.urls
        DispatchQueue.main.async {
            self.moviesClient.downloadImageforURLAndSetImageView(url: urls[indexPath.row],imageView: moviePosterImageView)
        }
        
        cell.addSubview(moviePosterImageView)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width - 50, height: frame.height - 10)
    }
    
    //small buffer on each side
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
}

/**
 Creates custom small movie poster cell. Includes custom inner UIcollectionview for smaller movie posters.
 */
class FeaturedCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var moviesClient: MoviesClient!
    private let cellId = "movieCell"
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.moviesClient = MoviesClient.init()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //Set movie topic name label displaying the topic using randoom set of topic names
    let nameLabel: UILabel = {
        let label = UILabel()
        var movieTopics = ["Scary","Don't Watch","Thriller","Awful","Oh No!","AH!","AHH"]
        var random = arc4random_uniform(UInt32(movieTopics.count - 1))
        label.text = movieTopics[Int(random)]
        label.font = UIFont.systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //Set internal collectionview properties
    let moviesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    /**
     Adds contraints needed and intiializes inner Blank cell to add movies
     */
    func setupViews() {
        backgroundColor = UIColor.clear
        addSubview(moviesCollectionView)
        addSubview(nameLabel)
        
        moviesCollectionView.dataSource = self
        moviesCollectionView.delegate = self
        moviesCollectionView.register(BlankCell.self, forCellWithReuseIdentifier: cellId)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": nameLabel]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": moviesCollectionView]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[nameLabel(30)][v0]|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["v0": moviesCollectionView,  "nameLabel": nameLabel]))
        
    }
    
    // MARK: Inner collectionView delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    
    /**
     Adds image view to blank cell and add corner radius.
     asynchonrously download image to view.
     Access movie client data to do this via shared instance.
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BlankCell
        let moviePosterImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
        moviePosterImageView.contentMode = .scaleAspectFill
        moviePosterImageView.layer.cornerRadius = 10
        moviePosterImageView.layer.masksToBounds = true
        moviePosterImageView.backgroundColor = UIColor.black
        var urls = SavedImageURLS.sharedInstance.urls
        var superViewTag = collectionView.superview?.tag
        superViewTag = superViewTag! * collectionView.numberOfItems(inSection: indexPath.section)
        DispatchQueue.main.async {
            // Perform your async code here
            self.moviesClient.downloadImageforURLAndSetImageView(url: urls[indexPath.row + superViewTag!],imageView: moviePosterImageView)
        }
        cell.addSubview(moviePosterImageView)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 105, height: frame.height-15)
    }
    
    //small buffer on each side
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
}



/**
 Blank UICollectionViewCell for adding imageviews and other elements to.
 */
class BlankCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
