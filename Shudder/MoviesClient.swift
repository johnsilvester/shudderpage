//
//  MoviesClient.swift
//  Shudder
//
//  Created by John Silvester on 11/15/18.
//  Copyright © 2018 John Silvester. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

/**
 Small singleton for passing data to hard to reach spots such as elements with multiple collectionviews
 */
class SavedImageURLS{
    var urls: [String] = []
    
    class var sharedInstance : SavedImageURLS {
        struct Static {
            static let instance : SavedImageURLS = SavedImageURLS()
        }
        return Static.instance
    }
}

/**
 API Related interactions are all done through Movies Client Class
 */
class MoviesClient: NSObject {
    
    private var flickrImageArray: [String] = []
    func fetchMovies(completion: @escaping (_ movies:[String]) -> ()){
        refreshFlickrImage(completion: {movies in
            if (movies.count > 0){
                completion(movies)}
            else{
                completion([])
            }
        })
    }
    
    
    /**
     Grabs most recent images from flickr api
     
     Completion - returns list of movies in URLs as String Format
     */
    func refreshFlickrImage(completion: @escaping (_ movies:[String]) -> ()) {
        var serverURL = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"
        Alamofire.request(serverURL, method: .get, parameters: nil, encoding:
            
            URLEncoding.default, headers: nil).responseJSON{
                
                response in switch response.result {
                    
                case .success(let JSON):
                    
                    let response = JSON as! NSDictionary
                    let flickrDataArray = response.object(forKey: "items") as! NSArray
                    for element in flickrDataArray{
                        
                        let json = element as! NSDictionary
                        let mediaImageArray = json.object(forKey: "media") as! NSDictionary
                        let mediaDataURL = mediaImageArray.object(forKey: "m") as? String
                        if mediaDataURL != nil{
                            
                            self.flickrImageArray.append(mediaDataURL!)
                            
                        }
                    }
                    completion(self.flickrImageArray)
                case.failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
        
    }
    
    /**
     Grabs actual image asynchronously
     
     Sets imageview to requested imageview on a UICollectionViewCell
     */
    func downloadImageforURLAndSetImageView(url: String, imageView:UIImageView){
        
        let urlString = URL(string:url)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: urlString!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                imageView.image = UIImage(data: data!)!
                
            }
        }
        
        
    }
    
    
    
}
